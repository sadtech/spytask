package ru.spytask.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Properties;

/**
 * Точка входа в приложение, тут поднимается контекст спринга.
 *
 * @author Konstantin
 */
public class SpringLauncher {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/taskBean.xml");
        Properties properties = (Properties) context.getBean("appConfig");
    }
}
