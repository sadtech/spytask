package ru.spytask.core.nospring.workers;

import ru.spytask.core.common.workers.CopyWorker;
import ru.spytask.core.common.workers.ReportWorker;
import ru.spytask.core.nospring.config.AppConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ScanWorker implements Runnable {
    private Properties appProperties;
    private boolean isRunning = false;

    public ScanWorker() {
        appProperties = new AppConfig().getResource();
        if (appProperties != null) {
            isRunning = true;
        }
    }

    public void run() {
        while (isRunning) {
            try {
                List<String> reportList = new ArrayList<>();

                scanDirectory(Paths.get(appProperties.getProperty("scan.directory")), reportList);

                if (Boolean.valueOf(appProperties.getProperty("copy.enabled"))) {
                    new CopyWorker(
                            Integer.valueOf(appProperties.getProperty("copy.thread.count")),
                            appProperties.getProperty("copy.directory"),
                            reportList
                    );
                }

                if (Boolean.valueOf(appProperties.getProperty("report.enabled"))) {
                    new ReportWorker(
                            appProperties.getProperty("report.name.prefix"),
                            appProperties.getProperty("report.name.postfix"),
                            reportList
                    );
                }

                System.out.println(String.join("", "\n", "Сканирование завершено, повторение...", "\n"));
                Thread.currentThread().sleep(Long.valueOf(appProperties.getProperty("scan.interval.sec")));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private TargetDescription getTargetDescription() {
        String kbSize = appProperties.getProperty("scan.file.size.kb");
        String mbSize = appProperties.getProperty("scan.file.size.mb");
        String gbSize = appProperties.getProperty("scan.file.size.gb");
        if (kbSize != null) {
            return new TargetDescription(kbSize, 1024, "КБ");
        } else if (mbSize != null) {
            return new TargetDescription(mbSize, 1024 * 1024, "МБ");
        } else if (gbSize != null) {
            return new TargetDescription(gbSize, 1024 * 1024 * 1024, "ГБ");
        } else {
            return new TargetDescription(String.valueOf(0), 1, "байт");
        }
    }

    private void scanDirectory(Path path, List<String> reportList) {
        TargetDescription targetDescription = getTargetDescription();
        try {
            Files.newDirectoryStream(path).forEach(item -> {
                File file = item.toAbsolutePath().toFile();
                if (file.isDirectory()) {
                    scanDirectory(item, reportList);
                } else {
                    if (file.length() >= targetDescription.getTargetSize()) {
                        if (file.getAbsolutePath().endsWith(appProperties.getProperty("scan.file.extension"))) {
                            reportList.add(item.toFile().getAbsolutePath());
                            System.out.println(item.toFile().getAbsolutePath() + " - размер файла: " + file.length() / targetDescription.getByteOffset() + targetDescription.getDesc());
                        }
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class TargetDescription {
        private long size;
        private long byteOffset;
        private String desc;

        public TargetDescription(String size, long byteOffset, String desc) {
            this.size = Long.valueOf(size);
            this.byteOffset = byteOffset;
            this.desc = desc;
        }

        public long getSize() {
            return size;
        }

        public long getByteOffset() {
            return byteOffset;
        }

        public String getDesc() {
            return desc;
        }

        public long getTargetSize() {
            return size * byteOffset;
        }
    }
}
