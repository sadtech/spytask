package ru.spytask.core.nospring.processing;

import ru.spytask.core.nospring.workers.ScanWorker;

public class TaskManager {
    public TaskManager() {
        new ScanWorker().run();
    }
}
