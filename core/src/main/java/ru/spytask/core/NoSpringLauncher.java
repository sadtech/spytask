package ru.spytask.core;

import ru.spytask.core.nospring.processing.TaskManager;

/**
 * Точка входа в приложение, тут поднимается контекст спринга.
 *
 * @author Konstantin
 */
public class NoSpringLauncher {
    public static void main(String[] args) {
        new TaskManager();
    }
}
