package ru.spytask.core.common.workers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReportWorker {
    public static final String CLASSPATH = new File(ReportWorker.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    public ReportWorker(String prefix, String postfix, List<String> reportList) {
        try {
            Files.write(
                    Paths.get(CLASSPATH, prefix + DATE_FORMAT.format(new Date()) + postfix),
                    String.join("\n", reportList).getBytes()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
