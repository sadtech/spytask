package ru.spytask.core.common.workers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CopyWorker {
    public CopyWorker(int threadCount, String targetDir, List<String> reportList) {
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        for (String reportItem: reportList) {
            executorService.submit(()-> {
                try {
                    Files.copy(
                            Paths.get(reportItem),
                            new FileOutputStream(Paths.get(targetDir).toFile().getAbsolutePath() + reportItem.substring(reportItem.lastIndexOf('\\')))
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
